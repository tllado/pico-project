#include <stdio.h>
#include "pico/stdlib.h"

const uint32_t LED_PIN{PICO_DEFAULT_LED_PIN};
const uint32_t LED_OFF{0U};
const uint32_t LED_ON{1U};
const uint32_t PERIOD{1000U};

int main() {
  // Initialize serial IO
  stdio_init_all();

  // Initialize Onboard LED
  gpio_init(LED_PIN);
  gpio_set_dir(LED_PIN, GPIO_OUT);

  // Run forever
  while (true) {
    gpio_put(LED_PIN, LED_ON);
    printf("LED ON\n");
    sleep_ms(PERIOD/2U);

    gpio_put(LED_PIN, LED_OFF);
    printf("LED OFF\n");
    sleep_ms(PERIOD/2U);
  }
}
