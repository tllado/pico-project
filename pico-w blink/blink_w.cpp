#include <stdio.h>
#include "pico/stdlib.h"
#include "pico/cyw43_arch.h"

const uint32_t LED_PIN{CYW43_WL_GPIO_LED_PIN};
const uint32_t LED_OFF{0U};
const uint32_t LED_ON{1U};
const uint32_t PERIOD{1000U};

int main() {
  //Initialize serial output
  stdio_init_all();

  // Initialize radio
  if (cyw43_arch_init()) {
		return -1;
	}

  // Run forever
  while (true) {
    printf("LED ON\n");
    cyw43_arch_gpio_put(LED_PIN, LED_ON);
    sleep_ms(PERIOD/2U);

    printf("LED OFF\n");
    cyw43_arch_gpio_put(LED_PIN, LED_OFF);
    sleep_ms(PERIOD/2U);
  }
}
