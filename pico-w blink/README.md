# Pico Blink_W

A minimal project for the Raspberry Pi Pico-W. It makes the onboard LED blink and should build with a single click using VSCode. The only external requirement is that the pico-sdk be installed at ~/rpi-pico/pico-sdk.
